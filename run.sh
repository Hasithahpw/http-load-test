#!/bin/bash

#########################################################
# This is simple shell script to simulate API load      #
# for given duration                                    #
# Eg: DURATION=60 and TPS=20, This script will trigger  #
# 20 requests per second for 1 minute.                  #
#########################################################

#define variables

set -x # run in debug mode
DURATION=60 # how long should load be applied ? - in seconds
TPS=20 # number of requests per second
end=$((SECONDS+$DURATION))
#start load
while [ $SECONDS -lt $end ];
do
        for ((i=1;i<=$TPS;i++)); do
                curl -X POST <url> -H 'Accept: application/json' -H 'Authorization: Bearer xxxxxxxxxxxxx' -H 'Content-Type: application/json' -d '{}' --cacert /path/to/cert/cert.crt -o /dev/null -s -w '%{time_starttransfer}\n' >> response-times.log &
        done
        sleep 1
done
wait
#end load
echo "Load test has been completed" 